package ke.co.davidwanjohi.kiswahilitoenglishconverter.fragments;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ke.co.davidwanjohi.kiswahilitoenglishconverter.Language;
import ke.co.davidwanjohi.kiswahilitoenglishconverter.MainActivity;
import ke.co.davidwanjohi.kiswahilitoenglishconverter.R;

public class ImportFragment extends Fragment {
    Button importBtn,nextBtn;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.import_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        importBtn=view.findViewById(R.id.import_btn);
        nextBtn=view.findViewById(R.id.next_btn);
        progressBar=view.findViewById(R.id.progress_bar);

        importBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                loadCsv();

            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction=((MainActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame,new HomeFragment(),"home").commit();
            }
        });
    }

    public void loadCsv(){

        String mCSVfile = "kisw_to_eng.csv";
        AssetManager manager = getActivity().getAssets();
        InputStream inStream = null;
        try {
            inStream = manager.open(mCSVfile);
        } catch (IOException e) {
            e.printStackTrace();

            Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG).show();

        }

        BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));

        String line = "";
        try {
            while ((line = buffer.readLine()) != null) {
                String[] colums = line.split(",");
                if (colums.length != 2) {
                    Log.d("CSVParser", "Skipping Bad CSV Row");
                    continue;
                }

                Language language=new Language();
                language.kiswahili=colums[0].trim();
                language.english=colums[1].trim();

                new insertAsyncTask().execute(language);
            }


        } catch (IOException e) {
            e.printStackTrace();

            Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG).show();

        }

        progressBar.setVisibility(View.GONE);

    }

    private static class insertAsyncTask extends AsyncTask<Language,Void,Void> {

        public insertAsyncTask() {

        }

        @Override
        protected Void doInBackground(Language... languages) {
//            if (new Select().from(Language.class).execute().size()!=0){
//
//                //first delete any existing data if there is saved data in the db
//                new Delete().from(Language.class).execute();
//            }
            ActiveAndroid.beginTransaction();
            try{

                for (Language language: languages){

                    Log.e("saving",language.english);
                    language.save();
                }
                ActiveAndroid.setTransactionSuccessful();
            }finally {
                ActiveAndroid.endTransaction();

            }

            return null;
        }

    }
}
