package ke.co.davidwanjohi.kiswahilitoenglishconverter;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table( name = "language")
public class Language extends Model {

    // If name is omitted, then the field name is used.
    @Column(name = "kiswahili")
    public String kiswahili;

    @Column(name = "english")
    public String english;
}
